#!/usr/bin/env bash
set -e

# echo an error message before exiting
trap 'echo "\"${last_command}\" command filed with exit code $?."' EXIT

if [[ $(whoami) = "gdk" ]]
then
  set -x
  pushd $HOME/gitlab-development-kit
  if [[ ! -d "$HOME/gitlab-development-kit/gitlab" ]]
  then
    sudo apt remove -y git-lfs || /bin/true
    git clone https://github.com/onecommons/gitlab-oc.git --branch handover --depth 1 gitlab
  fi
  if [[ ! -d "$HOME/gitlab-development-kit/services" ]]
  then
    pushd gitlab
    yarn install
    bundle install
    popd
    gdk install &
  else
    gdk start &
  fi
  popd
fi

